package inhouse.employeesearch.repository;

import inhouse.employeesearch.models.EmployeeRecord;
import inhouse.employeesearch.util.SearchMethod;

import java.util.List;

/**
 * Generic repository interface for Employee Data
 * This is used in the MainController context as the main search entry point. Implementation classes actually provide
 * the functionality. See EmployeeDbRepo
 */
public interface EmployeeRepository {

    List<EmployeeRecord> searchEmployees(String query, SearchMethod method);
}
