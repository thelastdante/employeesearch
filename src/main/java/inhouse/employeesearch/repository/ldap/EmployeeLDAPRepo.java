package inhouse.employeesearch.repository.ldap;

import inhouse.employeesearch.models.EmployeeRecord;
import inhouse.employeesearch.repository.EmployeeRepository;
import inhouse.employeesearch.util.SearchMethod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.naming.NamingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Implement this class for your entry point into searching LDAP.
 * The rest of the application will work as expected
 */
public class EmployeeLDAPRepo implements EmployeeRepository {

    private Logger log;
    private LDAPAccessor accessor;
    
    public EmployeeLDAPRepo() {
        String url = "ldap://foo.bar.com";
        String passwd = "bar";
        String shadow = "ad\\ldap";
        String base = "OU=Users,dc=foo,dc=bar,dc=com";

        log = LoggerFactory.getLogger("LDAP Repository");

        try {
            // since this opens a connection to a server, if this object is created on the main thread, hangups may occur
            accessor = new LDAPAccessor(url, passwd, shadow, base);
        } catch (NamingException e) {
            e.printStackTrace();
            log.error("Obtaining LDAP access failed. Expect strange results.");
            return;
        }
    }

    @Override
    public List<EmployeeRecord> searchEmployees(String query, SearchMethod method) {
        log.info("LDAP Employee search requested");
        List<EmployeeRecord> results = new ArrayList<>();

        try {
            switch (method) {
                case LASTNAME:
                    return accessor.fetchByName(query);
                case USERNAME:
                    return accessor.fetchByUsername(query);
                case EMPLOYEEID:
                    return accessor.fetchByEmployeeID(query);
                default:
                    log.warn("Somehow a search method was not passed to repo, expect an empty list");
                    break;
            }
        } catch (NamingException e) {
            e.printStackTrace();
            log.error("General failure fetching records");
        }
        return results;
    }
}
