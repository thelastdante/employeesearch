package inhouse.employeesearch.repository.ldap;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import sun.java2d.pipe.SpanShapeRenderer;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**************************
 * Created By: J.Medlock  *
 * Created On: 03/02/2018 *
 **************************/
public class DateConvert {
    private long date;

    public DateConvert() {
    }

    public long getDate() {
        return date;
    }

    public String setDate(String date) {
//        convertEpoch(date);
        return date;
    }

    public static String parseLdapDate(String ldapDate, String extraTime){
        long nanoseconds = Long.parseLong(ldapDate);
        long secondsConvert = Long.parseLong(extraTime);

        long mills = (nanoseconds / 10000000);

        long unix = (((1970 - 1601) * 365) - 3 + Math.round((1970 - 1601) / 4)) * 86400L;

        long timeStamp = (mills + secondsConvert) - unix;

        Date date = new Date(timeStamp * 1000L);
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss z");
        String formattedDate = sdf.format(date);

        return formattedDate;
    }

    public static String changeDateInfo(String time){
        String time_s = time;
        LocalDateTime datetime = LocalDateTime.parse(time_s, DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
        String newTime = datetime.format(DateTimeFormatter.ofPattern("MM-dd-yyyy"));
        return newTime;
    }
}

