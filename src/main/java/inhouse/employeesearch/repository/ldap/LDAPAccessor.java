package inhouse.employeesearch.repository.ldap;

import inhouse.employeesearch.models.EmployeeRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.*;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Date;

public class LDAPAccessor {

    private Logger log;

    private String host;
    private String password;
    private String shadow;
    private String searchBase;

    private Hashtable<String, String> ldapEnv;
    private DirContext ldapContext;
    private SearchControls searchControl;


    private static final String requestAttrs[] = {"cn", "title", "whenCreated", "whenChanged", "department", "company", "badPwdCount",
            "employeeID", "lastLogoff", "lastLogon", "pwdLastSet", "logonCount", "sAMAccountName", "lastLogonTimestamp",
            "manager", "mail", "pager",};


    private String manager;

    public LDAPAccessor(String host, String pw, String shadow, String base) throws NamingException {

        log = LoggerFactory.getLogger("LDAPAccessor");

        this.host = host;
        this.password = pw;
        this.shadow = shadow;
        this.searchBase = base;

        // Required data structures
        ldapEnv = new Hashtable<>(11);
        initLdap();
        Date date = new Date();
    }

    public List<EmployeeRecord> fetchByUsername(String user) throws NamingException {
        String searchFilter = "(&(sAMAccountName=" + user + "))";
        return searchRequest(searchFilter);
    }

    public List<EmployeeRecord> fetchByName(String name) throws NamingException {
        String searchFilter = "(&(sn=" + name + "))";
        return searchRequest(searchFilter);
    }

    public List<EmployeeRecord> fetchByEmployeeID(String ID) throws NamingException {
        String searchFilter = "(&(employeeID=" + ID + "))";
        return searchRequest(searchFilter);
    }

    private List<EmployeeRecord> searchRequest(String filter) throws NamingException {
        ArrayList<EmployeeRecord> result = new ArrayList<>();
        NamingEnumeration<SearchResult> response = null;

        try {
            response = ldapContext.search(searchBase, filter, searchControl);
        } catch (NamingException e) {
            e.printStackTrace();
            log.error("Unable to search using LDAP Context");
        }

        if (response == null) {
            return result; // return empty list, program will not crash and the UI can work with that
        }

        while (response.hasMore()) {
            SearchResult searchResult = response.next();

            try {
                result.add(mapObject(searchResult.getAttributes()));
            } catch (NamingException e) {
                e.printStackTrace();
                log.error("Error mapping object. Leaving search request function");
                break; // stop exec of the loop. Return what we have
            }
        }

        return result;
    }

    private EmployeeRecord mapObject(Attributes attrs) throws NamingException {
        EmployeeRecord record = new EmployeeRecord();
        Date date = new Date();

        try {

            // TIME RELATED fetches
            String createDate = attrs.get("whenCreated").get().toString().substring(0, 14);
            String changeDate = attrs.get("whenChanged").get().toString().substring(0, 14);
            String passwdChange = attrs.get("pwdLastSet").get().toString();
            //        String lastLogOff = attrs.get("lastLogoff").get().toString();
            String lastLogOn = attrs.get("lastLogon").get().toString();
            String lastLoginAttempt = attrs.get("lastLogonTimestamp").get().toString();
            String logonAttempt = DateConvert.parseLdapDate(lastLoginAttempt, "0");
            record.setLastLoginAttempt(logonAttempt);
            String pwdExpire = DateConvert.parseLdapDate(passwdChange, "8640000");


            //Date Conversion
            String createDateTime = DateConvert.changeDateInfo(createDate);
            String changeDateTime = DateConvert.changeDateInfo(changeDate);
            String changePwd = DateConvert.parseLdapDate(passwdChange, "0");
            String logOnLast = DateConvert.parseLdapDate(lastLogOn, "0");

            record.setCreateDate(createDateTime);
            record.setChangeDate(changeDateTime);
            record.setChangePwd(changePwd);
            record.setLastLogOn(logOnLast);
            record.setExpiredPwd(pwdExpire);

            record.setName(attrs.get("cn").get().toString());
            record.setUsername(attrs.get("samAccountName").get().toString());
            record.setEmployeeID(attrs.get("employeeID").get().toString());
            record.setTitle(attrs.get("title").get().toString());
            record.setDepartment(attrs.get("department").get().toString());
            record.setEmail(attrs.get("mail").get().toString()); //TODO refactor EmployeeRecord to handle mail?
            record.setLicense(attrs.get("pager").get().toString()); //TODO refactor EmployeeRecord to handle office license?
            record.setCompany(attrs.get("company").get().toString());
            // attrs.get("badPwdCount").get().toString() TODO refactor EmployeeRecord to handle bad password count?
            //        record.setLogonCount(Integer.parseInt(attrs.get("logonCount").get().toString()));
            manager = record.setManager(attrs.get("manager").get().toString());
            manager = manager.substring(manager.indexOf("=") + 1, manager.indexOf(","));
            record.setManager(manager);
        } catch (NullPointerException e){

            // TIME RELATED fetches
            String createDate = attrs.get("whenCreated").get().toString().substring(0, 14);
            String changeDate = attrs.get("whenChanged").get().toString().substring(0, 14);
            String passwdChange = attrs.get("pwdLastSet").get().toString();

            String changePwd = DateConvert.parseLdapDate(passwdChange, "0");
            String pwdExpire = DateConvert.parseLdapDate(passwdChange, "8640000");

            //Date Conversion
            String createDateTime = DateConvert.changeDateInfo(createDate);
            String changeDateTime = DateConvert.changeDateInfo(changeDate);
//            String passwdExpire = DateConvert.changeDateInfo(pwdExpire);

            record.setCreateDate(createDateTime);
            record.setChangeDate(changeDateTime);
//            record.setExpiredPwd(passwdExpire);

            record.setName(attrs.get("cn").get().toString());
            record.setUsername(attrs.get("samAccountName").get().toString());
            record.setEmployeeID(attrs.get("employeeID").get().toString());
            record.setTitle(attrs.get("title").get().toString());
            record.setDepartment(attrs.get("department").get().toString());
            record.setEmail(attrs.get("mail").get().toString()); //TODO refactor EmployeeRecord to handle mail?
            record.setLicense(attrs.get("pager").get().toString()); //TODO refactor EmployeeRecord to handle office license?
            record.setCompany(attrs.get("company").get().toString());
            // attrs.get("badPwdCount").get().toString() TODO refactor EmployeeRecord to handle bad password count?
            //        record.setLogonCount(Integer.parseInt(attrs.get("logonCount").get().toString()));
            manager = record.setManager(attrs.get("manager").get().toString());
            manager = manager.substring(manager.indexOf("=") + 1, manager.indexOf(","));
            record.setManager(manager);
        }

        return record;
    }

    private void initLdap() throws NamingException {
        ldapEnv.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        ldapEnv.put(Context.PROVIDER_URL, this.host);
        ldapEnv.put(Context.SECURITY_AUTHENTICATION, "simple");
        ldapEnv.put(Context.SECURITY_PRINCIPAL, shadow);
        ldapEnv.put(Context.SECURITY_CREDENTIALS, this.password);

        ldapContext = new InitialDirContext(ldapEnv);

        searchControl = new SearchControls();
        searchControl.setReturningAttributes(requestAttrs);
        searchControl.setSearchScope(SearchControls.SUBTREE_SCOPE);

    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        // Currently this will keep the connection open during the life of the object.
        // Plus side to this is that you don't need to reconnect on each search
        // TODO consider closing and re-opening for each search
        ldapContext.close();
    }
}
