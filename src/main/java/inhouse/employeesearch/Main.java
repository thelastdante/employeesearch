package inhouse.employeesearch;

import inhouse.employeesearch.concurrent.ExecutorHolder;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("/views/MainDialog.fxml"));
        primaryStage.setTitle("Employee Search");
        primaryStage.getIcons().add(new Image("/images/icon.jpeg"));
        primaryStage.setScene(new Scene(root, 707, 600));
        primaryStage.show();
    }


    public static void main(String[] args) {
        // create executor singleton
        ExecutorHolder.getInstance();

        launch(args);
    }
}
