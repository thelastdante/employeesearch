package inhouse.employeesearch.controllers;

import inhouse.employeesearch.models.EmployeeRecord;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.stage.Stage;

/**
 * Controller for the Detail stage. All this does is simple databinding.
 */
public class DetailController {

    private EmployeeRecord record;

    // UI references
    @FXML Label name;
    @FXML Label employeeID;
    @FXML Label username;
    @FXML Label title;
    @FXML Label department;
    @FXML Label email;
    @FXML Label license;
    @FXML Label manager;
    @FXML Label company;
    @FXML Label lastLogOn;
    @FXML Label lastLogOff;
    @FXML Label lastLoginAttempt;
//    @FXML Label logonCount;
    @FXML Label lastPwChange;
    @FXML Label createDate;
    @FXML Label changeDate;
    @FXML Label expiredPwd;


    @FXML
    protected void initialize() {

    }

    public void setRecord(EmployeeRecord record) {
        this.record = record;
        bindUiData();
    }

    private void bindUiData() {
        if (record != null) {
            name.setText(record.getName());
            employeeID.setText(String.valueOf(record.getEmployeeID()));
            username.setText(record.getUsername());
            title.setText(record.getTitle());
            department.setText(record.getDepartment());
            email.setText(record.getEmail());
            license.setText(record.getLicense());
            manager.setText(record.getManager());
            company.setText(record.getCompany());
            createDate.setText(record.getCreateDate());
            changeDate.setText(record.getChangeDate());
            expiredPwd.setText(record.getExpiredPwd());

            if (record.getChangePwd() == null){
                lastPwChange.setText("No Password Reset");
            } else {
                lastPwChange.setText(record.getChangePwd().toString());
            }

            if (record.getLastLogOn() == null){
                lastLogOn.setText("Never Logged On");
            } else {
                lastLogOn.setText(record.getLastLogOn().toString());
            }

            if (record.getLastLoginAttempt() == null){
                lastLoginAttempt.setText("Never Logged On");
            } else {
                lastLoginAttempt.setText(record.getLastLoginAttempt().toString());
            }

        }
    }

    public void handleCloseAction(ActionEvent event) {
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        stage.close();
    }
}
