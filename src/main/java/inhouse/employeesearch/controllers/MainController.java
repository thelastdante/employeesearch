package inhouse.employeesearch.controllers;

import inhouse.employeesearch.concurrent.ExecutorHolder;
import inhouse.employeesearch.models.EmployeeRecord;
import inhouse.employeesearch.repository.EmployeeRepository;
import inhouse.employeesearch.repository.ldap.EmployeeLDAPRepo;
import inhouse.employeesearch.util.SearchMethod;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.EventListener;
import java.util.List;

/**
 * MainController handles the main stage
 * TODO refactor this class into a more modular approach. This should ONLY be responsible for the view.
 * But for demo purposes this works well
 */
public class MainController {

    @FXML private ToggleGroup searchByToggle;
    @FXML private TextField lastNameField;
    @FXML private TextField usernameField;
    @FXML private TextField employeeIdField;
    @FXML private RadioButton lastNameRadio;
    @FXML private RadioButton usernameRadio;
    @FXML private RadioButton employeeRadio;

    // Table related
    @FXML private TableView<EmployeeRecord> dataTable;
    @FXML private TableColumn<EmployeeRecord, String> colName;
    @FXML private TableColumn<EmployeeRecord, String> colUsername;
    @FXML private TableColumn<EmployeeRecord, Integer> colEmployeeId;
    @FXML private TableColumn<EmployeeRecord, String> colTitle;
    @FXML private TableColumn<EmployeeRecord, String> colDepartment;

    private ObservableList<EmployeeRecord> employeeList;
    private EmployeeRepository dataRepo;

    private Logger log;

    public MainController() {
        log = LoggerFactory.getLogger("MainController");

        // the SPECIFIC TRUE SOURCE of data that is being searched
        // Change this to the implementation that pulls the data of interest
        dataRepo = new EmployeeLDAPRepo();
    }

    @FXML
    protected void initialize() {
        initTable();
        initToggleListener();
        initFieldEvents();
    }

    /**
     * Fired from button event
     * triggers an async search
     * @param event
     */
    @FXML
    public void handleSearchAction(ActionEvent event) {
        SearchMethod method = resolveCurrentSearchMethod();
        String query = "";
        log.info("Search initiated");

        if (method == null) {
            log.warn("Search method could not be resolved");
            return;
        }

        switch (method) {
            case EMPLOYEEID:
                query = employeeIdField.getText();
                break;
            case USERNAME:
                query = usernameField.getText();
                break;
            case LASTNAME:
                query = lastNameField.getText();
                break;
        }

        // trigger search
        asyncSearch(query, method);
    }



    public void handleClearAction(ActionEvent event) {
        log.info("Clear initiated");

        lastNameField.setText("");
        usernameField.setText("");
        employeeIdField.setText("");
        employeeList.clear();
    }

    /**
     * Searches the requested information on another thread
     * @param query text to be searched
     * @param method how we are searching
     */
    private void asyncSearch(String query, SearchMethod method) {

        // lambda expression that executes on another thread
        // thread is obtained from an executor instance, see ExecutorHolder
        ExecutorHolder.getInstance().getDefaultExecutor().execute(() -> {
            List<EmployeeRecord> result;

            result = dataRepo.searchEmployees(query, method);
            if (result.isEmpty()) {
                // todo logic here to update UI that no employees have been found
                log.warn("Employees have not been found using the current search terms");
                return;
            }
            employeeList.clear(); // in case we have a previous result
            employeeList.addAll(result);
        });
    }

    /**
     * Determines which search method is selected
     * @return SearchMethod
     */
    private SearchMethod resolveCurrentSearchMethod() {
        switch ((String) searchByToggle.getSelectedToggle().getUserData()) {
            case "lastname":
                return SearchMethod.LASTNAME;
            case "username":
                return SearchMethod.USERNAME;
            case "emp_id":
                return SearchMethod.EMPLOYEEID;
        }
        log.error("Incorrect search method was somehow selected. Investigate MainController.resolveCurrentSearchMethod");
        return null;
    }

    /**
     * Launches the window for the detailed view of a record
     * @param record
     */
    private void launchDetailWindow(EmployeeRecord record) {
        log.info("launching detail window");
        AnchorPane pane = null;
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/views/DetailDialog.fxml"));
        Scene detailScene;
        Stage detailStage;

        DetailController detailController;

        try {
            pane = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        detailController = (DetailController) loader.getController();
        detailController.setRecord(record);

        detailScene = new Scene(pane, 580, 391);
        detailStage = new Stage();
        detailStage.setScene(detailScene);

        detailStage.showAndWait();
    }


    // -----------------------------
    // Various init methods
    // -----------------------------
    private void initFieldEvents() {

        // when focused
        lastNameField.focusedProperty().addListener((value, hasOld, isNew) -> {
            if(isNew)
                lastNameRadio.setSelected(true);
        });
        usernameField.focusedProperty().addListener((value, hasOld, isNew) -> {
            if(isNew)
                usernameRadio.setSelected(true);
        });
        employeeIdField.focusedProperty().addListener((value, hasOld, isNew) -> {
            if (isNew)
                employeeRadio.setSelected(true);
        });

        log.debug("Field events initialized");
    }

    private void initToggleListener() {
        searchByToggle.selectedToggleProperty().addListener((observableValue, oldToggle, newToggle) -> {
            RadioButton newRadio = (RadioButton) newToggle;
            RadioButton oldRadio = (RadioButton) oldToggle;

            // set field state
            switch (newRadio.getId()) {
                case "lastNameRadio":
                    usernameField.clear();
                    employeeIdField.clear();
                    lastNameField.requestFocus();
                    break;
                case "usernameRadio":
                    lastNameField.clear();
                    employeeIdField.clear();
                    usernameField.requestFocus();
                    break;
                case "employeeRadio":
                    usernameField.clear();
                    lastNameField.clear();
                    employeeIdField.requestFocus();
                    break;
            }
        });

        log.debug("Toggle Listener initialized");
    }

    private void initTable() {
        employeeList = FXCollections.observableArrayList();
        dataTable.setItems(employeeList);

        initTableInteraction();
        initColumns();
    }

    /**
     * This function sets a factory class that is called every time a row in the table is added
     */
    private void initTableInteraction() {
        dataTable.setRowFactory( tv -> {
            TableRow<EmployeeRecord> row = new TableRow<>();
            row.setOnMouseClicked( event -> {
                if (event.getClickCount() == 2) {
                    launchDetailWindow(row.getItem());
                }
            });
            return row;
        });

        log.debug("Table row factory initialized");
    }

    /**
     * This function sets up the databinding for each column
     */
    private void initColumns() {
        colName.setCellValueFactory(new PropertyValueFactory<EmployeeRecord, String>("name"));
        colTitle.setCellValueFactory(new PropertyValueFactory<EmployeeRecord, String>("title"));
        colDepartment.setCellValueFactory(new PropertyValueFactory<EmployeeRecord, String>("department"));
        colEmployeeId.setCellValueFactory(new PropertyValueFactory<EmployeeRecord, Integer>("employeeID"));
        colUsername.setCellValueFactory(new PropertyValueFactory<EmployeeRecord, String>("username"));

        log.debug("Column databinds are initialized");
    }

}
