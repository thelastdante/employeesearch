package inhouse.employeesearch.models;

/**
 * Plain Old Java Object that represents the record of an employee
 */
public class EmployeeRecord {
    private String name;
    private String username;
    private String employeeID;
    private String title;
    private String department;
    private String manager;
    private String company;
    private String lastLogOff;
    private String lastLogOn;
    private String lastLoginAttempt;
    private int logonCount;
    private String email;
    private String license;
    private String createDate;
    private String changeDate;
    private String changePwd;
    private String expiredPwd;


    public EmployeeRecord() {

    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmployeeID() {
        return String.valueOf(employeeID);
    }

    public void setEmployeeID(String employeeID) {
        this.employeeID = employeeID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getManager() {
        return manager;
    }

    public String setManager(String manager) {
        this.manager = manager;
        return manager;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

//    public LocalDate getPasswdChange() {
//        return passwdChange;
//    }

//    public void setPasswdChange(LocalDate passwdChange) {
//        this.passwdChange = passwdChange;
//    }

    public String getLastLogOff() {
        return lastLogOff;
    }

    public void setLastLogOff(String lastLogOff) {
        this.lastLogOff = lastLogOff;
    }

    public String getLastLogOn() {
        return lastLogOn;
    }

    public void setLastLogOn(String lastLogOn) {
        this.lastLogOn = lastLogOn;
    }

    public String getLastLoginAttempt() {
        return lastLoginAttempt;
    }

    public void setLastLoginAttempt(String lastLoginAttempt) {
        this.lastLoginAttempt = lastLoginAttempt;
    }

//    public int getLogonCount() {
//        return logonCount;
//    }

    public void setLogonCount(int logonCount) {
        this.logonCount = logonCount;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getChangeDate() {
        return changeDate;
    }

    public void setChangeDate(String changeDate) {
        this.changeDate = changeDate;
    }

    public String getChangePwd() {
        return changePwd;
    }

    public void setChangePwd(String changePwd) {
        this.changePwd = changePwd;
    }

    public String getExpiredPwd() {
        return expiredPwd;
    }

    public void setExpiredPwd(String expiredPwd) {
        this.expiredPwd = expiredPwd;
    }
}
