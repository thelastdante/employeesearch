package inhouse.employeesearch.concurrent;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * Holder object for concurrent executor objects
 * A holder is used so that one executor can be shared across different components of the application.
 * This holder can only be instantiated by calling getInstance()
 */
public class ExecutorHolder {

    private static ExecutorHolder instance;

    private Executor defaultExecutor;

    private ExecutorHolder() {
        defaultExecutor = Executors.newSingleThreadExecutor();
    }

    public static ExecutorHolder getInstance() {

        // if an instance of this class does not exist, create one
        if(instance == null)
            instance = new ExecutorHolder();

        // and then return it
        return instance;
    }

    public Executor getDefaultExecutor() {
        return defaultExecutor;
    }
}
