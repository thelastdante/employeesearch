package inhouse.employeesearch.util;

/**
 * Defined search methods
 */
public enum SearchMethod {
    LASTNAME,
    USERNAME,
    EMPLOYEEID
}
