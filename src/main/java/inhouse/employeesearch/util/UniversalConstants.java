package inhouse.employeesearch.util;

/**
 * @author Cody Huzarski <chuzarski@gmail.com> (Tassit)
 */
public class UniversalConstants {

    public static final String JDBC_DB_URL = "jdbc:sqlite:/Users/tassit/IdeaProjects/EmployeeSearch/employees.sqlite";
}
