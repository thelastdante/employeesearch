-- Schema declaration for employees table

CREATE TABLE employees
(
    id INT PRIMARY KEY NOT NULL,
    name TEXT NOT NULL,
    username TEXT,
    title TEXT NOT NULL,
    department TEXT NOT NULL,
    manager TEXT NOT NULL,
    company TEXT NOT NULL,
    password_change_date DATETIME NOT NULL,
    last_log_off DATETIME NOT NULL,
    last_log_on DATETIME NOT NULL,
    last_login_attempt DATETIME,
    logon_count INT NOT NULL
);
CREATE UNIQUE INDEX employees_id_uindex ON employees (id);