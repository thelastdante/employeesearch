# Employee Search
Basic application to search a simple dataset of employees

# Building

### Step 1 Database
* Create an sqlite database and execute the query inside of schema.sql
* Populate the database with data
* Change the path in contained in `inhouse.employeesearch.util.UniversalConstants.JDBC_DB_URL` to point to the SQLite DB

### Step 2 Build
Run the following in the root project tree

`
./gradlew installDist
`

If there are no errors, we should be good to rock and roll

### Step 3 Run
Execute the following script

`
<projectRoot>/build/install/bin/EmployeeSearch
`